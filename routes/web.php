<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('createLead', 'LeadController@create');
Route::get('brands', 'CarsController@getCarBrands');
Route::get('marks/{markId}', 'CarsController@getModels')->where('markId', '[0-9]+');;
