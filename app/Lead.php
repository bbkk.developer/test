<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    protected $table = 'leads';

    protected $fillable = [
        'car_brand',
        'car_model',
        'name',
        'phone',
        'email'
    ];
}
