<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LeadRequest extends JsonRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand' => 'string',
            'model' => 'string',
            'name' => 'string',
            'email' => 'string|unique:leads,email',
            'phone' => 'string|unique:leads,phone'
        ];
    }

    public function messages()
    {
        return [
            'email.unique' => 'Почта уже используется',
            'phone.unique' => 'Телефон уже используется'
        ];
    }
}
