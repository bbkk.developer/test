<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LeadRequest;
use App\Lead;
use GuzzleHttp\Client;

class LeadController extends Controller
{
    public function create(LeadRequest $request)
    {
        $leadData = $request->validated();
        $lead = new Lead;
        $lead->car_brand = $leadData['brand'];
        $lead->car_model = $leadData['model'];
        $lead->name = $leadData['name'];
        $lead->email = $leadData['email'];
        $lead->phone = $leadData['phone'];
        $lead->save();
        $response = $this->sendLeadBitrix($lead);
        
        return response()->json(['status' => 200]);
    }

    public function sendLeadBitrix(Lead $lead)
    {
        $url =  'https://b24-5l7jlz.bitrix24.ua/crm/configs/import/lead.php';
        $loginData = [
            'login' => '+380637581001',
            'password' => 'testapi1'
        ];
        $client = new Client;

        $response = $client->post($url, [
            'form_params' => [
                'LOGIN' => $loginData['login'],
                'PASSWORD' => $loginData['password'],
                'TITLE' => $lead->name,
                'NAME' => $lead->name,
                'PHONE_WORK' =>  $lead->phone,
                'EMAIL_WORK' => $lead->email
            ]
        ]);
        
        return $response;
    }
}
