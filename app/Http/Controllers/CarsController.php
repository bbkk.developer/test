<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class CarsController extends Controller
{
    private $client;
    private $apiKey;
    
    public function __construct()
    {
        $this->client = new Client;
        $this->apiKey = env('AUTORIA_API_KEY');
    }

    public function getCarBrands()
    {
        $url =  "https://developers.ria.com/auto/categories/1/marks?api_key={$this->apiKey}";
        $response = $this->client->get($url);
        $brands = json_decode($response->getBody()->getContents(), true);
       
        return response()->json($brands);
    }

    public function getModels($markId)
    {
        $url =  "http://api.auto.ria.com/categories/1/marks/{$markId}/models?api_key={$this->apiKey}";
        $response = $this->client->get($url);
        $models = json_decode($response->getBody()->getContents(), true);
       
        return response()->json($models);
    }
}
