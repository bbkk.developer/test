require('./bootstrap');

window.Vue = require('vue');

import Vue from 'vue'
import Vuetify from 'vuetify'

import axios from 'axios'
Vue.prototype.$axios = axios;

Vue.use(Vuetify)

Vue.component(
    'welcome',
    require('./components/Welcome.vue').default
);

const app = new Vue({
    el: '#app',
    vuetify: new Vuetify(),
});

// window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';